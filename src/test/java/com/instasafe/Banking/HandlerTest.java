package com.instasafe.Banking;

import com.instasafe.Banking.config.RouterConfig;
import com.instasafe.Banking.model.Transaction;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class HandlerTest {
	
	@Autowired
	WebTestClient webTestClient;
	
	@Test
	public void postTest()  {
		Transaction transaction = new Transaction(BigDecimal.valueOf(1000), OffsetDateTime.now());
		
		webTestClient.post().uri("/transaction").contentType(MediaType.APPLICATION_JSON)
				.body(Mono.just(transaction), Transaction.class).exchange()
				.expectStatus().isCreated()
				.expectHeader().contentType(MediaType.APPLICATION_JSON);
		
		
	}
	
	@Test
	public void postTest2() {
		Transaction transaction1 = new Transaction(BigDecimal.valueOf(1000), OffsetDateTime.now().minusSeconds(61));
		webTestClient.post().uri("/transaction").contentType(MediaType.APPLICATION_JSON)
				.body(Mono.just(transaction1), Transaction.class).exchange()
				.expectStatus().isNoContent();
	}
	
	@Test
	public void getTest()   {
		webTestClient.get().uri("/statistics").exchange().expectStatus().isOk().expectBody().jsonPath("$.count").isEqualTo(0);
	}
	
	@Test
	public void deleteTest()    {
		webTestClient.delete().uri("/transaction").exchange().expectStatus().isNoContent();
	}
	
}
