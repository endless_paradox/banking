package com.instasafe.Banking.config;

import com.instasafe.Banking.BankingHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

@Configuration
@EnableWebFlux
public class RouterConfig {
	
	@Bean
	public RouterFunction<ServerResponse> getRoutes(BankingHandler bankingHandler)   {
		return RouterFunctions.route().POST("/transaction", bankingHandler::saveTransaction)
				.GET("/statistics", bankingHandler::getStats)
				.DELETE("/transaction", bankingHandler::deleteTransactions)
				.build();
	}
	
}
