package com.instasafe.Banking.service;

import com.instasafe.Banking.model.BigDecimalSummaryStatistics;
import com.instasafe.Banking.model.Statistics;
import com.instasafe.Banking.model.Transaction;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.math.MathContext;
import java.time.OffsetDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class BankingService {

	public List<Transaction> bankTransactionFlux;
	
	public Mono<Statistics> getStats()  {
		List<BigDecimal> getBigDecimals = bankTransactionFlux.stream().filter(e -> !e.getTimestamp().isBefore(OffsetDateTime.now().minusSeconds(60))).map(e -> e.getAmount()).collect(Collectors.toList());
		BigDecimalSummaryStatistics bds = getBigDecimals.stream().collect(BigDecimalSummaryStatistics.statistics());
		Statistics stats = new Statistics();
		stats.setMax(bds.getMax());
		stats.setMin(bds.getMin());
		stats.setAvg(bds.getAverage(MathContext.DECIMAL128));
		stats.setCount(bds.getCount());
		stats.setSum(bds.getSum());
		return Mono.just(stats);
	}
	
}
