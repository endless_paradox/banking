package com.instasafe.Banking;

import com.instasafe.Banking.model.Statistics;
import com.instasafe.Banking.model.Transaction;
import com.instasafe.Banking.service.BankingService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.time.OffsetDateTime;

@Component
@RequiredArgsConstructor
public class BankingHandler {
	
	private final BankingService bankingService;
	
	public Mono<ServerResponse> saveTransaction(ServerRequest serverRequest)  {
		return serverRequest.bodyToMono(Transaction.class).flatMap(transaction -> {
			if(transaction.getAmount() == null || transaction.getTimestamp() ==  null) {
				throw new RuntimeException("bad request");
			}
			this.bankingService.bankTransactionFlux.add(transaction);
			if(transaction.getTimestamp().isBefore(OffsetDateTime.now().minusSeconds(60)))  {
				throw new RuntimeException("no content");
			}
			return ServerResponse.created(URI.create("flux")).contentType(MediaType.APPLICATION_JSON).body(Mono.just(transaction), Transaction.class);
		}).onErrorResume(e -> {
					if(e.getMessage().equals("bad request"))   {
						return ServerResponse.badRequest().build();
					}
					else if(e.getMessage().equals("no content"))    {
						return ServerResponse.noContent().build();
					}
					return ServerResponse.unprocessableEntity().build();
				});
	}
	
	public Mono<ServerResponse> getStats(ServerRequest serverRequest)   {
		return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(this.bankingService.getStats(), Statistics.class);
	}
	
	public Mono<ServerResponse> deleteTransactions(ServerRequest serverRequest) {
		this.bankingService.bankTransactionFlux.clear();
		return ServerResponse.noContent().build();
	}
	
}
